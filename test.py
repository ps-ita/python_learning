import time
from datetime import datetime



class GateCounter(object):
    given_name = None
    family_name = None
    __DIRECTION_IN = 1
    __DIRECTION_OUT = 2
    __DIRECTIONS = {__DIRECTION_IN: "Enter", __DIRECTION_OUT: "Exit"}
    #move_counter = []

    def __init__(self, family_name=None, given_name=None ):
        self.family_name = family_name
        self.given_name = given_name
        self.move_counter = []
        print(f'Class initialised with family name: {self.family_name}, given name: {self.given_name}')
        return

    def get_fullname(self):
        #print('Ожидаем выполнение долгого сложного метода...')
        #time.sleep(5)
        fullname = f'{self.family_name} {self.given_name}'
        return fullname

    def move(self, direction):
        if direction == 1:
            direction = "Enter"
        else:
            direction = "Exit"

        # print(f'Сотрудник {self.family_name} {self.given_name} прошел через ворота.')
        now_time = datetime.now()
        formated_date = now_time.strftime("%d.%m.%Y")
        formated_time = now_time.strftime("%H:%M:%S")
        log_entry = {'date': formated_date,
                     'time': formated_time,
                     'fullname': f'{self.family_name} {self.given_name}',
                     'direction': direction}
        self.move_counter += [log_entry]
        return

    def counter_list(self):
        for entry in self.move_counter:
            print(f'{entry["date"]} {entry["time"]} {entry["fullname"]} {entry["direction"]}')

person1 = GateCounter(family_name='Ivanov', given_name='Vasilii')
person2 = GateCounter(family_name='Petrov', given_name='Sergey')
person3 = GateCounter(family_name='Grigoryeva', given_name='Evgeniya')

person1_fullname = person1.get_fullname()
print(person1_fullname)
person1.move(1)
person1.move(2)
person2.move(1)
person3.move(1)
person2.move(2)
person3.move(2)
person3.move(1)
person3.move(2)
person1.move(1)
person2.move(2)

person3.counter_list()

# persons = person1, person2, person3
#
# def counter_list():
#     for person in persons:
#         person.counter_list()
#
# counter_list()






